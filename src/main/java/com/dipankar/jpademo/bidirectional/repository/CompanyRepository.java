package com.dipankar.jpademo.bidirectional.repository;

import com.dipankar.jpademo.bidirectional.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

}
