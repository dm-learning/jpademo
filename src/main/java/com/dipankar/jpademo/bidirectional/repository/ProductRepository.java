package com.dipankar.jpademo.bidirectional.repository;

import com.dipankar.jpademo.bidirectional.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
