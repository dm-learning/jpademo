package com.dipankar.jpademo.bidirectional.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="company")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Product> products;

    public Company(){
    }

    public Company(String name){
        this.name = name;
    }

    public Company(String name, List<Product> products){
        this.name = name;
        this.products = products;
    }

    // name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // products
    public void setProducts(List<Product> products){
        this.products = products;
    }

    public List<Product> getProducts(){
        return this.products;
    }

//    public String toString(){
//        String info = "";
//        JSONObject jsonInfo = new JSONObject();
//        jsonInfo.put("name",this.name);
//
//        JSONArray productArray = new JSONArray();
//        if(this.products != null){
//            this.products.forEach(product->{
//                JSONObject subJson = new JSONObject();
//                subJson.put("name", product.getName());
//                productArray.put(subJson);
//            });
//        }
//        jsonInfo.put("products", productArray);
//        info = jsonInfo.toString();
//        return info;
//    }
}
