package com.dipankar.jpademo.bidirectional.controller;

import com.dipankar.jpademo.bidirectional.entity.Company;
import com.dipankar.jpademo.bidirectional.entity.Product;
import com.dipankar.jpademo.bidirectional.repository.CompanyRepository;
import com.dipankar.jpademo.bidirectional.repository.ProductRepository;
import com.dipankar.jpademo.bidirectional.service.CompanyService;
import com.dipankar.jpademo.bidirectional.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DemoController {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    ProductRepository productRepository;

    @PostMapping(value = "/company")
    public void addCustomer(@RequestBody Company company){
        companyRepository.save(company);

        company.getProducts().forEach(p -> {
            productRepository.save(p);
        });
    }

//    @PostMapping(value = "/product")
//    public void addCustomer(@RequestBody Product product){
//        productService.save(product);
//    }
//
//    @GetMapping("/company")
//    public List<Company> showCompany(){
//        System.out.println("Called ");
//        return companyService.showData();
//    }

}
