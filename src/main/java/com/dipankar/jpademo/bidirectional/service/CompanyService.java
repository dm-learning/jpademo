package com.dipankar.jpademo.bidirectional.service;

import com.dipankar.jpademo.bidirectional.entity.Company;
import com.dipankar.jpademo.bidirectional.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    @Transactional
    public Company save(Company company){
        return companyRepository.save(company);
    }

    @Transactional
    public List<Company> showData(){
        System.out.println("=====================Retrieve Companies from Database:====================");
        List<Company> companyLst = companyRepository.findAll();

        System.out.println("=====================Done:====================");
        return companyLst;
    }

    public void deleteAll(){
        companyRepository.deleteAll();
    }

}
