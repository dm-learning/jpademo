package com.dipankar.jpademo.bidirectional.service;

import com.dipankar.jpademo.bidirectional.entity.Product;
import com.dipankar.jpademo.bidirectional.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Transactional
    public Product save(Product product){
        return productRepository.save(product);
    }

    @Transactional
    public List<Product> showData(){
        System.out.println("=====================Retrieve Products from Database:====================");
        List<Product> productLst = productRepository.findAll();
        return productLst;

    }

    public void deleteAll(){
        productRepository.deleteAll();
    }
}
