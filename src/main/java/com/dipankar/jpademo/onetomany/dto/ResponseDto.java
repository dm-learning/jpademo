package com.dipankar.jpademo.onetomany.dto;

public class ResponseDto {

    private String accountNo;
    private String name;
    private String accountType;
    private double balance;

    public ResponseDto() {
    }

    public ResponseDto(String accountNo, String name, String accountType, double balance) {
        this.accountNo = accountNo;
        this.name = name;
        this.accountType = accountType;
        this.balance = balance;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
