package com.dipankar.jpademo.onetomany.controller;

import com.dipankar.jpademo.onetomany.dto.ResponseDto;
import com.dipankar.jpademo.onetomany.entity.Account;
import com.dipankar.jpademo.onetomany.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/account/{accountNo}")
    public Account getAccountInfo(@PathVariable String accountNo){
        return accountService.getAccountInfo(accountNo);
    }

    @GetMapping("/account/details/{accountNo}")
    public List<ResponseDto> getAccountDetails(@PathVariable String accountNo){
        return accountService.getAccountDetails(accountNo);
    }

}
