package com.dipankar.jpademo.onetomany.controller;

import com.dipankar.jpademo.onetomany.dto.ResponseDto;
import com.dipankar.jpademo.onetomany.entity.Customer;
import com.dipankar.jpademo.onetomany.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping(value = "/customer")
    public Customer addCustomer(@RequestBody Customer customer){
            return customerService.saveCustomer(customer);
    }

    @GetMapping (value = "/customer")
    public List<Customer> getAllCustomer(){
        return customerService.getAllCustomer();
    }

//    @GetMapping (value = "/account/details/{accountNo}")
//    public List<ResponseDto> getAllCustomer(@PathVariable String accountNo){
//        return customerService.getAccountDetails(accountNo);
//    }

}
