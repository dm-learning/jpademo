package com.dipankar.jpademo.onetomany.repository;

import com.dipankar.jpademo.onetomany.dto.ResponseDto;
import com.dipankar.jpademo.onetomany.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {

    public Account findByAccountNo(String accountNo);

    @Query("select new com.dipankar.jpademo.onetomany.dto.ResponseDto(c.name, a.accountNo, a.accountType, a.balance) from Customer c join c.accounts a where a.accountNo = ?1")
    public List<ResponseDto> getAccountDetails(String accountNo);

}
