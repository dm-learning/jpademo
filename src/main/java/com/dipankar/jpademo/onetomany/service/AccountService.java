package com.dipankar.jpademo.onetomany.service;

import com.dipankar.jpademo.onetomany.dto.ResponseDto;
import com.dipankar.jpademo.onetomany.entity.Account;
import com.dipankar.jpademo.onetomany.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account getAccountInfo(String accountNo){
        return accountRepository.findByAccountNo(accountNo);
    }

    public List<ResponseDto> getAccountDetails(String accountNo){
        return accountRepository.getAccountDetails(accountNo);
    }
}
