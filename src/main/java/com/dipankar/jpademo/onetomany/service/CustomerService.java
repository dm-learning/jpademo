package com.dipankar.jpademo.onetomany.service;

import com.dipankar.jpademo.onetomany.dto.ResponseDto;
import com.dipankar.jpademo.onetomany.entity.Customer;
import com.dipankar.jpademo.onetomany.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer saveCustomer(Customer customer){
        return customerRepository.save(customer);
    }

    public List<Customer> getAllCustomer(){
        System.out.println("Getting customer information");
        List<Customer> customers = customerRepository.findAll();
        System.out.println("===== END =====");
        return customers;
    }

//    public List<ResponseDto> getAccountDetails(String accountNo){
//        return customerRepository.getAccountDetails(accountNo);
//    }

}
