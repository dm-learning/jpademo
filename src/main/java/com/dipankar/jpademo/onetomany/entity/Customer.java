package com.dipankar.jpademo.onetomany.entity;

import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Customer")
public class Customer {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String email;
    @OneToMany(targetEntity = Account.class, cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    @JoinColumn(name = "CUST_ID",referencedColumnName = "id")
    List<Account> accounts;

    public Customer() {
    }

    public Customer(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
