package com.dipankar.jpademo.onetoone.repository;

import com.dipankar.jpademo.onetoone.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,String> {
}
