package com.dipankar.jpademo.onetoone.repository;

import com.dipankar.jpademo.onetoone.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile,Integer> {
}
