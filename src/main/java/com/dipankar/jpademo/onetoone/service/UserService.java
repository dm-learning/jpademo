package com.dipankar.jpademo.onetoone.service;

import com.dipankar.jpademo.onetoone.entity.User;
import com.dipankar.jpademo.onetoone.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User saveUser(User user){
        return repository.save(user);
    }

    public User getUserName(String userId){
        System.out.println("======Getting user details=====");
        User user = repository.findById(userId).orElse(new User());
        System.out.println("======Getting user details END =====");
        System.out.println("======Returning the user =====");
        return user;
    }

}
