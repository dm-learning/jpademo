package com.dipankar.jpademo.onetoone.entity;

import javax.persistence.*;

@Entity
@Table(name ="USER")
public class User {

    @Id
    @Column(name = "USER_ID")
    private String userId;

    private String password;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private UserProfile userProfile;

    public User() {
    }

    public User(String userId, String password, UserProfile userProfile) {
        this.userId = userId;
        this.password = password;
        this.userProfile = userProfile;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
