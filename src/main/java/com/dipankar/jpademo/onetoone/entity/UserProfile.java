package com.dipankar.jpademo.onetoone.entity;

import javax.persistence.*;

@Entity
@Table(name = "User_Profile")
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="PROFILE_ID")
    private int profileId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    private String name;

    private String status;


    public UserProfile(int profileId, User user, String name, String status) {
        this.profileId = profileId;
        this.user = user;
        this.name = name;
        this.status = status;
    }

    public UserProfile() {
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

//    public User getUser() {
//        return user;
//    }
//
    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
