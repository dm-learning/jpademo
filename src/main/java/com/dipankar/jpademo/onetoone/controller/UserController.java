package com.dipankar.jpademo.onetoone.controller;

import com.dipankar.jpademo.onetoone.entity.User;
import com.dipankar.jpademo.onetoone.entity.UserProfile;
import com.dipankar.jpademo.onetoone.repository.UserProfileRepository;
import com.dipankar.jpademo.onetoone.repository.UserRepository;
import com.dipankar.jpademo.onetoone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private UserService userService;

    @PostMapping (value = "/user")
    public User addUser(@RequestBody User user){

//        User newUser = new User();
//        newUser.setUserId(user.getUserId());
//        newUser.setPassword(user.getPassword());
//
//        UserProfile userProfile = user.getUserProfile();
//
//        userRepository.save(newUser);
//        userProfileRepository.save(userProfile);

        return userService.saveUser(user);

    }

    @GetMapping(value = "/user/{userId}")
    public User getUserDetails(@PathVariable String userId){
        return userService.getUserName(userId);
    }

}
