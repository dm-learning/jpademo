package com.dipankar.jpademo;

import com.dipankar.jpademo.bidirectional.entity.Company;
import com.dipankar.jpademo.bidirectional.entity.Product;
import com.dipankar.jpademo.bidirectional.service.CompanyService;
import com.dipankar.jpademo.bidirectional.service.ProductService;
import com.dipankar.jpademo.onetoone.entity.User;
import com.dipankar.jpademo.onetoone.entity.UserProfile;
import com.dipankar.jpademo.onetoone.repository.UserProfileRepository;
import com.dipankar.jpademo.onetoone.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class JpaDemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(JpaDemoApplication.class, args);
	}

	@Autowired
	CompanyService companyService;

	@Autowired
	ProductService productService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserProfileRepository userProfileRepository;

	@Override
	public void run(String... arg0) throws Exception {
//		clearData();
//		saveData();
//		showData();
//		saveUserData();
	}

	private void clearData(){
		System.out.println("=================== Clear DATA =======================");
		companyService.deleteAll();
		productService.deleteAll();
	}

	private void saveData(){
		System.out.println("=================== Save DATA =======================");
		Product iphone7 = new Product("Iphone 7");
		Product iPadPro = new Product("IPadPro");

		Product galaxyJ7 = new Product("GalaxyJ7");
		Product galaxyTabA = new Product("GalaxyTabA");

		Company apple = new Company("Apple1");
		Company samsung = new Company("Samsung1");

		// set company for products
		iphone7.setCompany(apple);
		iPadPro.setCompany(apple);

		galaxyJ7.setCompany(samsung);
		galaxyTabA.setCompany(samsung);

		// save companies
		companyService.save(apple);
		companyService.save(samsung);

		// save products
		productService.save(iphone7);
		productService.save(iPadPro);

		productService.save(galaxyJ7);
		productService.save(galaxyTabA);
	}

	private void showData(){
		System.out.println("=================== Show ALL Data =======================");
		companyService.showData();
		productService.showData();
	}

//	public void saveUserData(){
//
//		User user = new User();
//		user.setUserId("u1");
//		user.setPassword("p1");
//
//		UserProfile userProfile= new UserProfile();
//		userProfile.setName("n1");
//		userProfile.setStatus("s1");
//		userProfile.setUser(user);
//
//		userRepository.save(user);
//		userProfileRepository.save(userProfile);
//
//	}


}
